// Main Importes
import Vue from 'vue'
import Router from 'vue-router'
import VueAxios from 'vue-axios'
import axios from 'axios';
import jQuery from 'jquery';
import Interceptor from './services/Interceptor'
import BootstrapVue from 'bootstrap-vue'
import apiPath from './configApi'

// import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

//import font-awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import { faMap } from '@fortawesome/free-solid-svg-icons'
import { faBuilding } from '@fortawesome/free-solid-svg-icons'
import { faUser } from '@fortawesome/free-solid-svg-icons'
import { faPhone } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

//Baidu Map
import BaiduMap from 'vue-baidu-map'

library.add(faSearch)
library.add(faSignOutAlt)
library.add(faMap)
library.add(faBuilding)
library.add(faUser)
library.add(faPhone)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false


// CSS
require('bootstrap/dist/css/bootstrap.min.css');
require('./assets/css/loading.css');
require('./assets/css/loading-btn.css');
require('./assets/css/default.css');
require('./assets/css/buttons.css');
require('./assets/css/fonts.css');
require('./assets/css/style.css');
// JS
global.$ = jQuery;


// Components
import App from './App.vue'
import Dashboard from './components/Dashboard.vue'
import NotFound from './components/404.vue'
import Login from './components/Login.vue';
// import Navbar from './components/Navbar.vue';

// Vue.component( 'Navbar', Navbar);

Vue.config.productionTip = false;

axios.defaults.baseURL = apiPath.apiPath;

// Vue.use(NavBar);
Vue.use(Router);
Vue.use(VueAxios, axios);
Vue.use(Interceptor);
Vue.use(BootstrapVue);
Vue.use(BaiduMap, {
  /* Visit http://lbsyun.baidu.com/apiconsole/key for details about app key. */
  ak: 'YOUR_APP_KEY'
})
Vue.config.ignoredElements = ['doc-preview'];
const router = new Router({
	mode: 'history',
	routes: [
		{ path : '', name: 'login', component: Login },
		{ path: '/dashboard/:city?/:district?/:company?/:location?', component: Dashboard, name: 'dashboard' },
		{path: '*', component: NotFound, name: '404'},
		// {path: '/', component: Home, name: 'home', meta: {requiresVisitors: true}},
	]
});
/* eslint-disable */
router.beforeEach((to, from, next) => {
	// if (to.meta.requiresAuth1) {
	// 	const authUser = JSON.parse(window.localStorage.getItem('authUser'));
	// 	console.log(authUser)
	// 	if (authUser && authUser.access_token) {
	// 		next()
	// 	} else {
	// 		next({name: 'login'})
	// 	}
	// } else if (to.meta.requiresVisitors) {
	// 	const authUser = JSON.parse(window.localStorage.getItem('authUser'));
	// 	if (authUser && authUser.access_token) {
	// 		next({name: 'company'})
	// 	} else {
	// 		next()
	// 	}
	// }
	next()
});

new Vue({
	router,
	mode: 'history',
  render: h => h(App)
}).$mount('#app');


